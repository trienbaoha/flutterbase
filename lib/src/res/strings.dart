class Ids {
  static const String titleHome = 'title_home';
  static const String titleRepos = 'title_repos';
  static const String titleEvents = 'title_events';
  static const String titleSystem = 'title_system';

  static const String titleBookmarks = 'title_bookmarks';
  static const String titleCollection = 'title_collection';
  static const String titleSetting = 'title_setting';
  static const String titleAbout = 'title_about';
  static const String titleShare = 'title_share';
  static const String titleSignOut = 'title_signout';
  static const String titleLanguage = 'title_language';
  static const String titleTheme = 'title_theme';
  static const String titleAuthor = 'title_author';
  static const String titleOther = 'title_other';

  static const String languageAuto = 'language_auto';
  static const String languageVI = 'language_vi';
  static const String languageEN = 'language_en';

  static const String save = 'save';
  static const String more = 'more';

  static const String recRepos = 'rec_repos';
  static const String recWxArticle = 'rec_wxarticle';

  static const String titleReposTree = 'title_repos_tree';
  static const String titleWxArticleTree = 'title_wxarticle_tree';
  static const String titleSystemTree = 'title_system_tree';
}

Map<String, Map<String, String>> localizedSimpleValues = {
  'en': {
    Ids.titleHome: 'Home',
    Ids.titleRepos: 'Repos',
    Ids.titleEvents: 'Events',
    Ids.titleSystem: 'System',
    Ids.titleBookmarks: 'Bookmarks',
    Ids.titleSetting: 'Setting',
    Ids.titleAbout: 'About',
    Ids.titleShare: 'Share',
    Ids.titleSignOut: 'Sign Out',
    Ids.titleLanguage: 'Language',
    Ids.languageAuto: 'Auto',
  },
  'vi': {
    Ids.titleHome: 'Home',
    Ids.titleRepos: 'Repos',
    Ids.titleEvents: 'Events',
    Ids.titleSystem: 'System',
    Ids.titleBookmarks: 'Bookmarks',
    Ids.titleSetting: 'Setting',
    Ids.titleAbout: 'About',
    Ids.titleShare: 'Share',
    Ids.titleSignOut: 'Sign Out',
    Ids.titleLanguage: 'Language',
    Ids.languageAuto: 'Auto',
  },
};

Map<String, Map<String, Map<String, String>>> localizedValues = {
  'en': {
    'US': {
      Ids.titleHome: 'Home',
      Ids.titleRepos: 'Repos',
      Ids.titleEvents: 'Events',
      Ids.titleSystem: 'System',
      Ids.titleBookmarks: 'Bookmarks',
      Ids.titleCollection: 'Collection',
      Ids.titleSetting: 'Setting',
      Ids.titleAbout: 'About',
      Ids.titleShare: 'Share',
      Ids.titleSignOut: 'Sign Out',
      Ids.titleLanguage: 'Language',
      Ids.languageAuto: 'Auto',
      Ids.save: 'Save',
      Ids.more: 'More',
      Ids.recRepos: 'Reco Repos',
      Ids.recWxArticle: 'Reco WxArticle',
      Ids.titleReposTree: 'Repos Tree',
      Ids.titleWxArticleTree: 'Wx Article',
      Ids.titleTheme: 'Theme',
    }
  },
  'vi': {
    'VN': {
      Ids.titleHome: 'Home',
      Ids.titleRepos: 'Repos',
      Ids.titleEvents: 'Sự kiện',
      Ids.titleSystem: 'Hệ thống',
      Ids.titleBookmarks: 'Đánh dấu',
      Ids.titleCollection: 'Collection',
      Ids.titleSetting: 'Cài đặt',
      Ids.titleAbout: 'Giới thiệu',
      Ids.titleShare: 'Chia sẻ',
      Ids.titleSignOut: 'Đăng xuất',
      Ids.titleLanguage: 'Ngôn ngữ',
      Ids.languageAuto: 'Tự động',
      Ids.languageVI: 'Tiếng việt',
      Ids.languageEN: 'English',
      Ids.save: 'Lưu',
      Ids.more: 'Thêm',
      Ids.recRepos: 'Reco Repos',
      Ids.recWxArticle: 'Reco WxArticle',
      Ids.titleReposTree: 'Repos Tree',
      Ids.titleWxArticleTree: 'Wx Article',
      Ids.titleTheme: 'Giao diện',
    }
  }
};
